public class Main {

    public static void main(String[] args) {
        Integer digit=3;
        double bound=Math.pow(10,digit);
        double lbound=Math.pow(10,digit-1);
        double max=0;
        int maxi=0,maxj=0;
        for(int i=(int)bound-1;i>=lbound;i--)
        {
            for(int j=(int)bound-1;j>=lbound;j--)
            {
                Integer prod=i*j;
                String prodStr=prod.toString();
                if(Reverse(prodStr))
                {
                    if(prod>max)
                    {
                        max=prod;
                        maxi=i;
                        maxj=j;
                    }
                }
            }
        }
        System.out.println("A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.");
        System.out.println("Find the largest palindrome made from the product of two 3-digit numbers");
        System.out.println("(i:)" +maxi+" * (j:) "+maxj+ "= "+max);
        return;

    }
    public static boolean Reverse(String in)
    {
        char [] C=in.toCharArray();
        for(int i=0 ; i<C.length/2;i++)
        {
            if (C[i]!=C[C.length-i-1])
            {
                return false;
            }
        }
        return  true;
    }
}
