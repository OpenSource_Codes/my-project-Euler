/**
 * Created by sanaztaheri on 12/4/17.
 */
public class P6 {
    public static void main(String[] args)
    {

        int n=100;
        //1^2+...+m^2=m(m+1)(2m+1)/6
        int squared=n*(n+1)*(2*n+1)/6;

        //1+....+100=100(100+1)/2
        int sum=n*(n+1)/2;
        sum=sum*sum;

        System.out.println("Sum square difference");
        System.out.println("Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum");
        System.out.println(Math.abs(sum-squared));
    }
}
