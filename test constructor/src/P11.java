import java.io.File;
import java.util.*;

/**
 * Created by sanaztaheri on 12/6/17.
 */
public class P11 {
    public static void main(String[] args)
    {
        //reaf the file and fill the array
        ArrayList<ArrayList<Double>> A=new ArrayList<ArrayList<Double>>();
        try {
            File f = new File("input.txt");
            Scanner sc = new Scanner(f);
            int rw=-1;
            while(sc.hasNextLine())
            {
                String line=sc.nextLine();
                Scanner sc2=new Scanner(line);
                A.add(new ArrayList<Double>());
                rw++;
                while(sc2.hasNextInt())
                {
                    A.get(rw).add(sc2.nextDouble());
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        //start searching the grid
        double maxProd=0;
        int direction=0; //0 row, 1 col, 2 diag, 3 antidiagonal
        int indexC=0;
        int indexR=0;
        double col=0, row=0, diag=0, antidiag=0;
        for(int i=0;i<A.size();i++)//row
        {
            for(int j=0;j<A.get(0).size();j++)//col
            {
                System.out.println(i+" "+j);
                col=0;
                row=0;
                diag=0;
                antidiag=0;
                if(i+3<A.size())
                {
                    col=A.get(i).get(j)*A.get(i+1).get(j)*A.get(i+2).get(j)*A.get(i+3).get(j);
                }
                if(j+3<A.get(0).size())
                {
                    row=A.get(i).get(j)*A.get(i).get(j+1)*A.get(i).get(j+2)*A.get(i).get(j+3);
                }
                if(i+3<A.size() && j+3<A.get(0).size())
                {
                    diag=A.get(i).get(j)*A.get(i+1).get(j+1)*A.get(i+2).get(j+2)*A.get(i+3).get(j+3);
                }
                if(i-3>=0 && j-3>=0)
                {
                    antidiag=A.get(i).get(j)*A.get(i-1).get(j-1)*A.get(i-2).get(j-2)*A.get(i-3).get(j-3);
                }

                if(row>maxProd)
                {
                    maxProd=row;
                    direction=0;
                    indexR=i;
                    indexC=j;
                }
                if(col>maxProd)
                {
                    maxProd=col;
                    direction=1;
                    indexR=i;
                    indexC=j;
                }
                if(diag>maxProd)
                {
                    maxProd=diag;
                    direction=2;
                    indexR=i;
                    indexC=j;
                }
              /*  if(antidiag>maxProd)
                {
                    maxProd=antidiag;
                    direction=3;
                    indexR=i;
                    indexC=j;
                }*/

            }
        }
        System.out.println(A.toString());
        System.out.println(maxProd);
        System.out.println("Row: "+indexR+" Col:"+ indexC);
        System.out.println("Direction: "+direction);

    }
}
