import java.math.BigInteger;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by sanaztaheri on 12/8/17.
 */
public class P13 {
    public static void main(String [] args)
    {
        ArrayList<BigInteger> A=new ArrayList<>();
        BigInteger sum=new BigInteger("0");
        try{
            File f=new File("P13_Input");
            Scanner sc=new Scanner(f);
            while (sc.hasNextLine())
            {
                BigInteger b=new BigInteger(sc.nextLine());
                sum=sum.add(b);
                A.add(b);
            }
            System.out.println(sum.toString());
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
