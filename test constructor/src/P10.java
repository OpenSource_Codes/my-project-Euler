/**
 * Created by sanaztaheri on 12/5/17.
 */
public class P10 {
    public static void main(String[]args)
    {
        int bound=2000000;
        double sum=0;
        for(int i=2;i<bound;i++)
        {
            if(isPrime(i)){
                sum+=i;
            }
        }
        System.out.println("The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.\n" +
                "\n" +
                "Find the sum of all the primes below two million.");
        System.out.println("sum= "+sum);
    }
    public static boolean isPrime(double n)
    {
        for(int i=2;i<=Math.sqrt(n);i++)
        {
            if(n%i==0)
                return false;

        }
        return true;
    }
}
