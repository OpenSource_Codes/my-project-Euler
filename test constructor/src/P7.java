/**
 * Created by sanaztaheri on 12/5/17.
 */
public class P7 {
    public static void main(String [] args)
    {
        int bound=10001;
        int count=0;
        double num=1;
        while(count<bound)
        {
            num++;
            if(isPrime(num)) {
                //System.out.println(num);
                count++;
            }

        }
        System.out.println("By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.\n" +
                "\n" +
                "What is the 10 001st prime number?");
        System.out.println(num);
    }

    public static boolean isPrime(double n)
    {
        for(int i=2;i<=Math.sqrt(n);i++)
        {
            if(n%i==0)
                return false;

        }
        return true;
    }
}
