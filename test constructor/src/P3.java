import java.math.BigInteger;

/**
 * Created by sanaztaheri on 12/3/17.
 */
public class P3 {
    public static final BigInteger NUMBER=new BigInteger("600851475143");
    public static void main(String[] args)
    {
        BigInteger n=NUMBER;
        BigInteger i=new BigInteger("2");
        BigInteger max=new BigInteger("0");
        while((n.compareTo(i) > 0))
        {
            if (n.mod(i).compareTo(new BigInteger("0"))==0)
            {
                n=n.divide(i);
                System.out.println("i= "+ i+ "n= "+ n);
                if(i.compareTo(max)>0){
                    max=i;
                };
                i=i.add(new BigInteger("1"));
            }
            else i=i.add(new BigInteger("1"));;
        }
        if(n.compareTo(max)>0){
            max=n;
        }
        System.out.println("The prime factors of 13195 are 5, 7, 13 and 29.\n" +
                "\n" +
                "What is the largest prime factor of the number 600851475143 ? "+ max);
    }
}
