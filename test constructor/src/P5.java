/**
 * Created by stahe on 12/4/2017.
 */
public class P5 {
    public static void main(String[] args) {
        double res=20,gcd=1;
        for(double i=19;i>1;i--)
        {
            gcd=GCD(res,i);
            res=LCM(gcd,res,i);
        }
        System.out.println(res);

    }

    public static double GCD(double a,double b)
    {
        double rem,q,temp;
        while(a>0){
            rem=a%b;
            q=a/b;
            if(rem==0){
                return b;
            }
            temp=a;
            a=b;
            b=rem;
        }
        return 1;
    }
    public static double LCM(double gcd,double a, double b){
        return a*b/gcd;
    }
}
