/**
 * Created by sanaztaheri on 12/3/17.
 */
public class P251 {
    public static void main(String [] args)
    {
        int cnt=0;

        for(float a=0;a<=1000;a++)
        {
            for(float b=0;b<=1000-a;b++)
            {
                for(float c=0;c<=1000-a-b;c++){
                    double temp=(b*Math.sqrt(c));
                    if(Math.cbrt(a+temp)+Math.cbrt(a-temp)==1.0)
                    {
                        cnt++;
                    }

                }
            }
        }
        System.out.println(cnt);

    }
}
