/**
 * Created by sanaztaheri on 12/3/17.
 */
public class P2 {
    public static final int MAX_VALE=4000000;
    public static void main(String[]args)
    {
        int a1=1;
        int a2=2;
        int temp=0;
        int sum=0;
        while(a2<=MAX_VALE)
        {
            if(a2%2==0)
            {
                sum+=a2;
            }
            temp=a2;
            a2=a1+a2;
            a1=temp;
        }
        System.out.println("By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms: "+sum);
    }
}
