/**
 * Created by sanaztaheri on 12/5/17.
 */
public class P9 {

    public static void main(String[] args)
    {
        for(int c=1;c<=1000;c++)
        {
            for(int b=1;b<c;b++)
            {
                int a=1000-c-b;
                if(a>=b || a<0)
                    continue;
                if(a*a+b*b==c*c)
                {
                    System.out.println("A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,\n" +
                            "\n" +
                            "a2 + b2 = c2\n" +
                            "For example, 32 + 42 = 9 + 16 = 25 = 52.\n" +
                            "\n" +
                            "There exists exactly one Pythagorean triplet for which a + b + c = 1000.\n" +
                            "Find the product abc.\n");
                    System.out.println("a="+a+" b="+b+" c="+c);
                    System.out.println(a+" +"+b+" +"+c+" ="+(a+b+c));
                    System.out.println(a+"^2+"+b+"^2= "+c+"^2=" +(a*a+b*b));
                    System.out.println(a+"*"+b+"*"+c+"="+(a*b*c));
                }

                }
            }
        }
        //System.out.println();
}
